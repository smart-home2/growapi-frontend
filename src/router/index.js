import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard'
import Login from '../views/Login'
import Beds from '../views/Beds'
import Plants from '../views/Plants'
import GrowCycles from '../views/GrowCycles'
import AuthService from '../services/AuthService'
import Notifications from '../views/Notifications'
const authService = new AuthService();

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Index',
    component: Dashboard,
  },
  {
    path: '/Dashboard',
    name: 'Dashboard',
    component: Dashboard,
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/Beds',
    name: 'Beds',
    component: Beds
  },
  {
    path: '/Plants',
    name: 'Plants',
    component: Plants
  },
  {
    path: '/Notifications',
    name: 'Notifications',
    component: Notifications
  },
  {
    path: '/GrowCycles',
    name: 'GrowCycles',
    component: GrowCycles,
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && !authService.isAuthenticated())
    next({ name: 'Login' })
  else
    next()
})

export default router

export default class BedService {
    getBedsAsync(unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const get = async () => {
            const response = await fetch(`${basePath}/beds`, { headers: { 'Authorization': 'Bearer ' + token } })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.json()
        }

        return get()
    }

    deleteBedAsync(id, unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const del = async () => {
            const response = await fetch(`${basePath}/beds/${id}`, {
                 method: 'delete', 
                 headers: { 'Authorization': 'Bearer ' + token }
             })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.text()
        }

        return del()
    }

    addBedAsync(bed, unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const post = async () => {
            const response = await fetch(`${basePath}/beds`, {
                method: 'post', 
                headers: { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' },
                body: JSON.stringify(bed)
            })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.text()
        }

        return post()
    }

    updateBedAsync(bed, unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const put = async () => {
            const response = await fetch(`${basePath}/beds/${bed.id}`, {
                method: 'put', 
                headers: { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' },
                body: JSON.stringify(bed)
            })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.text()
        }

        return put()
    }
}
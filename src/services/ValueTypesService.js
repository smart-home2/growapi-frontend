export default class ValueTypesService {
    getValueTypesAsync(unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const get = async () => {
            const response = await fetch(`${basePath}/value-types`, { headers: { 'Authorization': 'Bearer ' + token } })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.json()
        }

        return get()
    }
}
export default class AuthService {
    getDecodedToken() {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            return false
        }
      
        let tokenParts = token.split('.')
        return JSON.parse(atob(tokenParts[1]))
    }

    isAuthenticated() {
        let expiryDate = new Date(this.getDecodedToken().exp * 1000)
      
        if (expiryDate < Date.now()) {
            return false
        }

        return true
    }

    getEmail() {
        let tokenBody = this.getDecodedToken()
        return tokenBody.email
    }

    getExp() {
        let tokenBody = this.getDecodedToken()
        return new Date(tokenBody.exp * 1000)
    }

    login(email, password, successCallback, failureCallback) {
        const basePath = process.env.VUE_APP_API_URL

        const post = async () => {
            const response = await fetch(`${basePath}/auth`, {
                method: 'POST',
                headers:
                {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ password: password, user: email })
            });
            
            if (response.ok) {
                try {
                    localStorage.setItem("jwtToken", (await response.json()).jwtToken);
                    successCallback()
                } catch (e) {
                    failureCallback(e)
                }
            }
            else {
                failureCallback(await response.text())
            }
        };
        post();
    }

    logout() {
        localStorage.removeItem("jwtToken");
    }
}
export default class GrowCyclesService {
    getGrowCyclesAsync(unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const get = async () => {
            const response = await fetch(`${basePath}/grow-cycles`, { headers: { 'Authorization': 'Bearer ' + token } })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.json()
        }

        return get()
    }

    deleteGrowCyclesAsync(id, unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const del = async () => {
            const response = await fetch(`${basePath}/grow-cycles/${id}`, {
                 method: 'delete', 
                 headers: { 'Authorization': 'Bearer ' + token }
             })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.text()
        }

        return del()
    }

    addGrowCycleAsync(bed, unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const post = async () => {
            const response = await fetch(`${basePath}/grow-cycles`, {
                method: 'post', 
                headers: { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' },
                body: JSON.stringify(bed)
            })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.text()
        }

        return post()
    }

    updateGrowCyclesAsync(bed, unauthorizedCallback, errorCallback) {
        let token = localStorage.getItem("jwtToken")

        if (!token) {
            unauthorizedCallback()
            return;
        }

        const basePath = process.env.VUE_APP_API_URL

        const put = async () => {
            const response = await fetch(`${basePath}/grow-cycles/${bed.id}`, {
                method: 'put', 
                headers: { 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' },
                body: JSON.stringify(bed)
            })
            
            if (!response.ok) {
                if (response.status === 401) {
                    unauthorizedCallback()
                } else {
                    errorCallback(response.status, await response.text())
                }

                return;
            }
            
            return await response.text()
        }

        return put()
    }
}
